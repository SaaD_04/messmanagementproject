<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Staff;
use DB;

class authController extends Controller
{
    //
    public function signup(Request $request){
        $request->validate([
            'name' => 'required|max:50',
            'email' => 'required|max:25',
            'password' => 'required',
            'phone' => 'required|max:15',
            'nid' => 'required|max:15',
            'f_name' => 'required|max:50',
            'm_name' => 'required|max:50',
            'thana' => 'required|max:10',
            'district' => 'required|max:10'
        ]);

        $data = $request->input();
        $staff = DB::table('staff')->where('phone', $data['phone'])->get();

        if($staff->isEmpty()){
            $staff = new Staff();

            $staff->name = $data['name'];
            $staff->email = $data['email'];
            $staff->password = $data['password'];
            $staff->phone = $data['phone'];
            $staff->NID = $data['nid'];
            $staff->father_name = $data['f_name'];
            $staff->mother_name = $data['m_name'];
            $staff->thana = $data['thana'];
            $staff->district = $data['district'];

            $staff->save();
            return response()->json(

                $staff
            );
        }
        else {
            return response()->json(
                'Staff exists with this email'
            );
        }
    }
}
