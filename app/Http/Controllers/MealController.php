<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Meal;
use DB;

class MealController extends Controller{
    //
    public function getAllMeal(Request $request){
        $meal = DB::table('meal')->get();

        return response()->json(
            $meal
        );
    }

    public function createMeal(Request $request){
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'time' => 'required',
            'date' => 'required',
            'price' => 'required'
        ]);

        $data = $request->input();

        $meal = new Meal();

        $meal->id = $data['id'];
        $meal->name = $data['name'];
        $meal->time = $data['time'];
        $meal->date = $data['date'];
        $meal->price = $data['price'];

        $meal->save();
        return response()->json($meal);
    }
}

