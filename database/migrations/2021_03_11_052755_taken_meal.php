<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TakenMeal extends Migration{

    public function up(){
        //
        Schema::create('takenMeal', function (Blueprint $table) {
            $table->integer('student_id');
            $table->integer('meal_id');
            $table->date('meal_date');
            $table->timestamps();
        });
    }

    public function down(){
        //
        Schema::dropIfExists('takenMeal');
    }
}
