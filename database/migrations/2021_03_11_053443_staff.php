<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Staff extends Migration{
    public function up(){
        //

        Schema::create('Staff', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('email', 25);
            $table->string('password');
            $table->string('phone', 15);
            $table->string('NID', 15);
            $table->string('father_name', 50);
            $table->string('mother_name', 50);
            $table->string('thana', 10);
            $table->string('district', 10);
            $table->timestamps();
        });
    }

    public function down(){
        //
        Schema::dropIfExists('Staff');
    }
}
