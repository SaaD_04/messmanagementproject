<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Meal extends Migration{

    public function up(){
        //

        Schema::create('Meal', function (Blueprint $table) {
            $table->integer('id');
            $table->string('name');
            $table->string('time');
            $table->date('date');
            $table->integer('price');
            $table->timestamps();
        });
    }

    public function down(){
        //
        Schema::dropIfExists('Meal');
    }
}
